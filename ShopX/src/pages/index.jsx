import { Page, Card, Navbar, Icon, Box, Text, Tabs, Link, Tab, Tabbar, useStore } from "zmp-framework/react";
import React from "react";
import Information from '../components/information'



const HomePage = () => {

    return (
    <Page name="home">
      <Information />
    </Page>
  );
}
export default HomePage;